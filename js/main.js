/**
 *  Blur the main stage to give colours effect
 */

$(function() {
	$('.blur-layer').blurjs({
		source: '.main-stage',
	    radius: 25,
	    overlay: ''
	});
});