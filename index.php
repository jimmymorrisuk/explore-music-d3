<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
    <title>Style Guide Boilerplate</title>
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700|Roboto:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet/less" type="text/css" href="css/site.less" />
    <!-- Optional but tasty -->
    <link rel="stylesheet/less" type="text/css" href="css/pine.less" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">  
    <script type="text/javascript">
        less = {
            env: "development", // or "production"
            async: false,       // load imports async
            fileAsync: false,   // load imports async when in a page under
                                // a file protocol
            poll: 1000,         // when in watch mode, time in ms between polls
            functions: {},      // user functions, keyed by name
            dumpLineNumbers: "comments", // or "mediaQuery" or "all"
            relativeUrls: false,// whether to adjust url's to be relative
                                // if false, url's are already relative to the
                                // entry less file
        };
    </script>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="js/less.js" type="text/javascript"></script>
    <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
  </head>
  <body>

    <header>
      <div class="wrapper">
        <div class="row">
          <div class="span6">
            <h1>Explore Music</h1>
          </div>
          <nav class="span6">
            <i class="fa fa-bars"></i>
            <ul>
              <li><a href="#related-artists">Related Artists</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
    <section id="related-artists" class="related-artists">
      <div class="row">
        <div class="span9">
          <h2>
            Enter your Last FM username
          </h2>
          <form id="usename-form" name="user">
            <input id="input-username" type="text" placeholder="Your Last FM username" />
            <select id="select-period">
              <option value="weekly">Weekly</option>
              <option value="top-artists">All time</option>
            </select>
            <input id="load-btn" type="submit" value="Juice me" /> 
          </form>
<script>

$(function () {
  var username;
  var period;
  $("#load-btn").click(function(e) {
    username = $(this).siblings("#input-username").val();
    period = $(this).siblings("#select-period").val();
    console.log("Form: " + username);
    console.log("Form: " + period);
    d3.select("svg")
       .remove();
    musicMap(username, period);
    e.preventDefault();
  });

  function musicMap(username, period) {
    if (username == "") {
      username = "jimbo_moses";
    }
    console.log(username);
    console.log(period);

    var width = 1000,
        height = 1000;
        method = "";

    var color = d3.scale.category20();

    var force = d3.layout.force()
        .charge(-120)
        .linkDistance(30)
        .size([width, height]);

    var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height);

    // Set up the JSON objects
    var nodes = {
        names: [],
        links: []
    };

    nodes.names.push({ 
        "name" : "You",
        "group" : 1
    });

    if (period == "weekly") {
      // This is the url method part of weekly artists
      method = "user.getWeeklyArtistChart";
    } else {
      // This is the url method part of top artists
      method = "user.gettopartists.gettopartists"; 
    }


    // Get the lenght for a counter check 
    var theArtistNumber = 20;
    // Number of related artists to add
    var relatedNumber = 12;
    // Get the users top played artists
    console.log(username);
    d3.json("http://ws.audioscrobbler.com/2.0/?method="+ method +"&user="+ username +"&api_key=be4ff3242bdb1d653517df99df39cfe2&format=json", function(error, graph) {
      // Loops through them and push them in nodes.names[]
      for (var i = 0; i < theArtistNumber; i++) { 
        // Get the first artist and add them to the object
        if (period == "weekly") {
          var item = graph.weeklyartistchart.artist[i];
        } else {
          var item = graph.topartists.artist[i];
        }
        //console.log("Artist: " + item.name + " i: " + i);
        nodes.names.push({ 
            "name" : item.name,
            "group" : 2
        });
        nodes.links.push({ 
            "source" : 0,
            "target" : i + 1
        });
        if (i == theArtistNumber - 1) {
          var theArtists = true;
        }
      }
      if (theArtists) {
        for (var i = 1; i < nodes.names.length;  i++) {
          //console.log("http://ws.audioscrobbler.com/2.0/?method=artist.getsimilar&artist="+ encodeURIComponent(nodes.names[i].name) +"&api_key=be4ff3242bdb1d653517df99df39cfe2&format=json")
          getRelatedItems(i, relatedNumber);
        }
      }

      function getRelatedItems(index, relatedNumber) {
        d3.json("http://ws.audioscrobbler.com/2.0/?method=artist.getsimilar&artist="+ encodeURIComponent(nodes.names[index].name) +"&api_key=be4ff3242bdb1d653517df99df39cfe2&format=json", function(error, related) {
          //console.log("INDEX " + index);
          for(i = 0; i < relatedNumber; i++) {  
            var relatedItem = related.similarartists.artist[i];
            // Add those to our json object like with top artists
            //console.log("Rel-Artist: " + relatedItem.name + " i: " + i);
            //console.log(relatedItem);
            nodes.names.push({ 
              "name" : relatedItem.name,
              "group" : 3
            });
            nodes.links.push({ 
              "source" : index,
              "target" : nodes.names.length - 1
            });
          }
          if ((relatedNumber * theArtistNumber) + (theArtistNumber + 1)  == nodes.names.length) {
            makeGraph();
          }
        });
      }

      function makeGraph() {
        force
            .nodes(nodes.names)
            .links(nodes.links)
            .distance(60)
            .start();

        // Added the links based on the data
        var link = svg.selectAll(".link")
            .data(nodes.links)
            .enter().append("line")
            .attr("class", "link")
            .style("stroke-width", function(d) { return Math.sqrt(d.value); });

        // Create the groups under svg
        var gnodes = svg.selectAll('.gnode')
          .data(nodes.names)
          .enter()
          .append('g')
          .classed({'gnode': true})
          .attr("class", function(d){return "group" + d.group;});

        // Add one circle in each group
        var node = gnodes.append("circle")
          .attr("class", "node")
          .attr("r", 8)
          .style("fill", function(d) { return color(d.group); })
          .call(force.drag);

        node.append("title")
            .text(function(d){return d.name;});

        // Append the labels to each group
        var labels = gnodes.append("text")
          .text(function(d) { return d.name; });
   

        force.on("tick", function() {
          link.attr("x1", function(d) { return d.source.x; })
              .attr("y1", function(d) { return d.source.y; })
              .attr("x2", function(d) { return d.target.x; })
              .attr("y2", function(d) { return d.target.y; });

          // Translate the groups
          gnodes.attr("transform", function(d) { 
            return 'translate(' + [d.x, d.y] + ')'; 
          });   
        });
        console.log("Finished");
      }      
    //   force
    //       .nodes(nodes.names)
    //       .links(nodes.links)
    //       .distance(40)
    //       .start();

    //   var link = svg.selectAll(".link")
    //       .data(nodes.links)
    //     .enter().append("line")
    //       .attr("class", "link")
    //       .style("stroke-width", function(d) { return Math.sqrt(d.value); });

    //   var node = svg.selectAll(".node")
    //       .data(nodes.names)
    //       .enter().append("circle")
    //       .attr("class", "node")
    //       .attr("class", function(d) { return d.group; })
    //       .attr("r", 10)
    //       .style("fill", function(d) { return color(d.group); })
    //       .call(force.drag);

    //   node.append("title")
    //       .text(function(d){return d.name;});
    //   node.append("data")
    //       .text(function(d){return d.group;});

    //   force.on("tick", function() {
    //     link.attr("x1", function(d) { return d.source.x; })
    //         .attr("y1", function(d) { return d.source.y; })
    //         .attr("x2", function(d) { return d.target.x; })
    //         .attr("y2", function(d) { return d.target.y; });

    //     node.attr("cx", function(d) { return d.x; })
    //         .attr("cy", function(d) { return d.y; });
    //   });
    });
  }

});
</script>
        </div>
        <div class="span3">

        </div>
      </div>
 
    </section>
  </body>

  </body>
</html>